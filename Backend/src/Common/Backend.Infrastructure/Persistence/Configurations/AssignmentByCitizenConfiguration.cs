using Backend.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Backend.Infrastructure.Persistence.Configurations
{
    public class AssignmentByCitizenConfiguration : IEntityTypeConfiguration<AssignmentByCitizen>
    {
        public void Configure(EntityTypeBuilder<AssignmentByCitizen> builder)
        {
            builder.Ignore(e => e.DomainEvents);
            builder.Property(t => t.AssignmentId)
                .HasMaxLength(100)
                .IsRequired();
            builder.Property(t => t.CitizenId)
                .HasMaxLength(200)
                .IsRequired();
            builder.Property(t => t.Status)
            .HasMaxLength(200)
            .IsRequired();
            builder.Property(t => t.Day)
                .IsRequired();
        }
    }
}
