using System.Collections.Generic;
using Backend.Domain.Entities;
using Mapster;

namespace Backend.Application.Dto
{
    public class AssignmentDto : IRegister
    {
        public AssignmentDto()
        {
        }

        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public bool Active { get; set; }
        public string CreateDate { get; set; }

        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Assignment, AssignmentDto>()
            .Map(dest => dest.CreateDate,
                src => $"{src.CreateDate.ToShortDateString()}");
        }
    }
}
