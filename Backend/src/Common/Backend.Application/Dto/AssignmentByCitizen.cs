using System.Collections.Generic;
using Backend.Domain.Entities;
using Mapster;

namespace Backend.Application.Dto
{
    public class AssignmentByCitizenDto : IRegister
    {
        public AssignmentByCitizenDto()
        {
        }

        public int Id { get; set; }
        public string CitizenId { get; set; }
        public string AssignmentId { get; set; }
        public string Status { get; set; }
        public int Day{get;set;}
        public string CreateDate { get; set; }

        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<AssignmentByCitizen, AssignmentByCitizenDto>()
            .Map(dest => dest.CreateDate,
                src => $"{src.CreateDate.ToShortDateString()}");
        }
    }
}
