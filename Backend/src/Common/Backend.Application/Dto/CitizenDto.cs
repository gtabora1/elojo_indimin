using System.Collections.Generic;
using Backend.Domain.Entities;
using Mapster;

namespace Backend.Application.Dto
{
    public class CitizenDto : IRegister
    {
        public CitizenDto()
        {
        }

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool Active { get; set; }
        public string CreateDate { get; set; }

        public string FullName { get; set; }

        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Citizen, CitizenDto>()
            .Map(dest => dest.CreateDate,
                src => $"{src.CreateDate.ToShortDateString()}");
        }
    }
}
