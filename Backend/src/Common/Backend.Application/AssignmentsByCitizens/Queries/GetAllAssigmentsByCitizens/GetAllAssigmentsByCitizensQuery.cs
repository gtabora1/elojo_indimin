using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace Backend.Application.AssigmentByCitizen.Queries.GetAssignmentsByCitizens
{
    public class GetAllAssignmentsByCitizensQuery : IRequestWrapper<List<AssignmentByCitizenDto>>
    {

    }

    public class GetAllAssignmentsByCitizensQueryHandler : IRequestHandlerWrapper<GetAllAssignmentsByCitizensQuery, List<AssignmentByCitizenDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllAssignmentsByCitizensQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<List<AssignmentByCitizenDto>>> Handle(GetAllAssignmentsByCitizensQuery request, CancellationToken cancellationToken)
        {
            List<AssignmentByCitizenDto> list = await _context.AssignmentByCitizens
                .ProjectToType<AssignmentByCitizenDto>(_mapper.Config)
                .ToListAsync(cancellationToken);

            return list.Count > 0 ? ServiceResult.Success(list) : ServiceResult.Failed<List<AssignmentByCitizenDto>>(ServiceError.NotFount);
        }
    }
}
