using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace Backend.Application.AssignmentsByCitizens.Queries.GetAssignmentsPerDay
{
    public class GetAssignmentsPerDayQuery : IRequestWrapper<AssignmentByCitizenDto>
    {
        public int Day { get; set; }
    }

    public class GetAssignmentsPerDayQueryHandler : IRequestHandlerWrapper<GetAssignmentsPerDayQuery, AssignmentByCitizenDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAssignmentsPerDayQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<AssignmentByCitizenDto>> Handle(GetAssignmentsPerDayQuery request, CancellationToken cancellationToken)
        {
            var assignment = await _context.AssignmentByCitizens
                .Where(x => x.Day == request.Day)
                .ProjectToType<AssignmentByCitizenDto>(_mapper.Config)
                .FirstOrDefaultAsync(cancellationToken);

            return assignment != null ? ServiceResult.Success(assignment) : ServiceResult.Failed<AssignmentByCitizenDto>(ServiceError.NotFount);
        }
    }
}
