using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Backend.Domain.Entities;
using Backend.Domain.Event;
using MapsterMapper;

namespace Backend.Application.AssignmentsByCitizens.Commands.Create
{
    public class CreateAssignmentByCitizenCommand : IRequestWrapper<AssignmentByCitizenDto>
    {
        public int CitizenId { get; set; }
        public int AssigmentId { get; set; }
        public int Day { get; set; }
    }

    public class CreateAssignmentByCitizenCommandHandler : IRequestHandlerWrapper<CreateAssignmentByCitizenCommand, AssignmentByCitizenDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public CreateAssignmentByCitizenCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<AssignmentByCitizenDto>> Handle(CreateAssignmentByCitizenCommand request, CancellationToken cancellationToken)
        {
            var entity = new AssignmentByCitizen
            {
              AssignmentId=request.AssigmentId,
              CitizenId=request.CitizenId,
              Day=request.Day,
              Status="PENDING"
            };

            entity.DomainEvents.Add(new AssignmentByCitizenCreatedEvent(entity));

            await _context.AssignmentByCitizens.AddAsync(entity, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            return ServiceResult.Success(_mapper.Map<AssignmentByCitizenDto>(entity));
        }
    }
}
