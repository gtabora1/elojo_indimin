using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Exceptions;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Backend.Domain.Entities;
using MapsterMapper;

namespace Backend.Application.AssignmentsByCitizens.Commands.Update
{
    public class UpdateAssignmentByCitizenCommand : IRequestWrapper<AssignmentByCitizenDto>
    {
        public int Id { get; set; }
        public int CitizenId { get; set; }
        public int AssignmentId { get; set; }
        public int Day { get; set; }
        public string Status{get;set;}
    }

    public class UpdateAssignmentByCitizenCommandHandler : IRequestHandlerWrapper<UpdateAssignmentByCitizenCommand, AssignmentByCitizenDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public UpdateAssignmentByCitizenCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<AssignmentByCitizenDto>> Handle(UpdateAssignmentByCitizenCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.AssignmentByCitizens.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Assignment), request.Id);
            }
            if (request.AssignmentId > 0)
                entity.AssignmentId = request.AssignmentId;
            if (request.CitizenId > 0)
                entity.CitizenId = request.CitizenId;
            if (request.Day > 0)
                entity.Day = request.Day;
                entity.Status=request.Status;

            await _context.SaveChangesAsync(cancellationToken);

            return ServiceResult.Success(_mapper.Map<AssignmentByCitizenDto>(entity));
        }
    }
}
