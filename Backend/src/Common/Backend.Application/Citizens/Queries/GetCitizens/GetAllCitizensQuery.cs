using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace ElOjo.Backend.Application.Citizens.Queries.GetCitizen
{
    public class GetAllCitizensQuery : IRequestWrapper<List<CitizenDto>>
    {

    }

    public class GetCitizensQueryHandler : IRequestHandlerWrapper<GetAllCitizensQuery, List<CitizenDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetCitizensQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<List<CitizenDto>>> Handle(GetAllCitizensQuery request, CancellationToken cancellationToken)
        {
            List<CitizenDto> list = await _context.Citizens
                .ProjectToType<CitizenDto>(_mapper.Config)
                .ToListAsync(cancellationToken);

            return list.Count > 0 ? ServiceResult.Success(list) : ServiceResult.Failed<List<CitizenDto>>(ServiceError.NotFount);
        }
    }
}
