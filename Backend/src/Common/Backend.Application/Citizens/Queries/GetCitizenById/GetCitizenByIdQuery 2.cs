using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace Backend.Application.Citizens.Queries.GetCitizenById
{
    public class GetCitizenByIdQuery : IRequestWrapper<CitizenDto>
    {
        public int Id { get; set; }
    }

    public class GetCitizenByIdQueryHandler : IRequestHandlerWrapper<GetCitizenByIdQuery, CitizenDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetCitizenByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<CitizenDto>> Handle(GetCitizenByIdQuery request, CancellationToken cancellationToken)
        {
            var citizen = await _context.Citizens
                .Where(x => x.Id == request.Id)
                .ProjectToType<CitizenDto>(_mapper.Config)
                .FirstOrDefaultAsync(cancellationToken);

            return citizen != null ? ServiceResult.Success(citizen) : ServiceResult.Failed<CitizenDto>(ServiceError.NotFount);
        }
    }
}
