using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Exceptions;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Backend.Domain.Entities;
using MapsterMapper;

namespace ElOjo.Backend.Application.Citizens.Commands.Update
{
    public class UpdateCitizenCommand : IRequestWrapper<CitizenDto>
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Active { get; set; }
    }

    public class UpdateCitizenCommandHandler : IRequestHandlerWrapper<UpdateCitizenCommand, CitizenDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public UpdateCitizenCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<CitizenDto>> Handle(UpdateCitizenCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Citizens.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Citizen), request.Id);
            }
            if (!string.IsNullOrEmpty(request.FirstName))
                entity.FirstName = request.FirstName;
            if (!string.IsNullOrEmpty(request.LastName))
                entity.LastName = request.LastName;
            entity.Active = request.Active;

            await _context.SaveChangesAsync(cancellationToken);

            return ServiceResult.Success(_mapper.Map<CitizenDto>(entity));
        }
    }
}
