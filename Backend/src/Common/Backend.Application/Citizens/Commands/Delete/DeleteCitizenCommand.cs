using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Exceptions;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Backend.Domain.Entities;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace ElOjo.Backend.Application.Citizens.Commands.Delete
{
    public class DeleteCitizenCommand : IRequestWrapper<CitizenDto>
    {
        public int Id { get; set; }
    }

    public class DeleteCitizenCommandHandler : IRequestHandlerWrapper<DeleteCitizenCommand, CitizenDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public DeleteCitizenCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<CitizenDto>> Handle(DeleteCitizenCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Citizens
                .Where(l => l.Id == request.Id)
                .SingleOrDefaultAsync(cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Citizen), request.Id);
            }

            _context.Citizens.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return ServiceResult.Success(_mapper.Map<CitizenDto>(entity));
        }
    }
}
