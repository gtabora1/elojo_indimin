using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace Backend.Application.Assignments.Queries.GetAssignmentById
{
    public class GetAssignmentByIdQuery : IRequestWrapper<AssignmentDto>
    {
        public int Id { get; set; }
    }

    public class GetAssignmentByIdQueryHandler : IRequestHandlerWrapper<GetAssignmentByIdQuery, AssignmentDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAssignmentByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<AssignmentDto>> Handle(GetAssignmentByIdQuery request, CancellationToken cancellationToken)
        {
            var assignment = await _context.Assignments
                .Where(x => x.Id == request.Id)
                .ProjectToType<AssignmentDto>(_mapper.Config)
                .FirstOrDefaultAsync(cancellationToken);

            return assignment != null ? ServiceResult.Success(assignment) : ServiceResult.Failed<AssignmentDto>(ServiceError.NotFount);
        }
    }
}
