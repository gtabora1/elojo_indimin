using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Exceptions;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Backend.Domain.Entities;
using MapsterMapper;

namespace Backend.Application.Assignments.Commands.Update
{
    public class UpdateAssignmentCommand : IRequestWrapper<AssignmentDto>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
    }

    public class UpdateAssignmentCommandHandler : IRequestHandlerWrapper<UpdateAssignmentCommand, AssignmentDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public UpdateAssignmentCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<AssignmentDto>> Handle(UpdateAssignmentCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Assignments.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Assignment), request.Id);
            }
            if (!string.IsNullOrEmpty(request.Title))
                entity.Title = request.Title;
            if (!string.IsNullOrEmpty(request.Description))
                entity.Description = request.Description;
            entity.Active = request.Active;

            await _context.SaveChangesAsync(cancellationToken);

            return ServiceResult.Success(_mapper.Map<AssignmentDto>(entity));
        }
    }
}
