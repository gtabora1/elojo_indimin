using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Exceptions;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Backend.Domain.Entities;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace ElOjo.Backend.Application.Assignments.Commands.Delete
{
    public class DeleteAssignmentCommand : IRequestWrapper<AssignmentDto>
    {
        public int Id { get; set; }
    }

    public class DeleteAssignmentCommandHandler : IRequestHandlerWrapper<DeleteAssignmentCommand, AssignmentDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public DeleteAssignmentCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<AssignmentDto>> Handle(DeleteAssignmentCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Assignments
                .Where(l => l.Id == request.Id)
                .SingleOrDefaultAsync(cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Assignment), request.Id);
            }

            _context.Assignments.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return ServiceResult.Success(_mapper.Map<AssignmentDto>(entity));
        }
    }
}
