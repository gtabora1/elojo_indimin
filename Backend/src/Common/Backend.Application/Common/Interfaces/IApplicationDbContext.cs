﻿using System.Threading;
using System.Threading.Tasks;
using Backend.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Backend.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<Citizen> Citizens { get; set; }
        DbSet<Assignment> Assignments { get; set; }
        DbSet<AssignmentByCitizen> AssignmentByCitizens{get;set;}
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
