using Backend.Domain.Common;
using Backend.Domain.Entities;

namespace Backend.Domain.Event
{
    public class AssignmentCreatedEvent : DomainEvent
    {
        public AssignmentCreatedEvent(Assignment assignment)
        {
            Assignment = assignment;
        }

        public Assignment Assignment { get; }
    }
}
