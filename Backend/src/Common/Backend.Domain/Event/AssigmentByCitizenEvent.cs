using Backend.Domain.Common;
using Backend.Domain.Entities;

namespace Backend.Domain.Event
{
    public class AssignmentByCitizenCreatedEvent : DomainEvent
    {
        public AssignmentByCitizenCreatedEvent(AssignmentByCitizen assignment)
        {
            Assignment = assignment;
        }

        public AssignmentByCitizen Assignment { get; }
    }
}
