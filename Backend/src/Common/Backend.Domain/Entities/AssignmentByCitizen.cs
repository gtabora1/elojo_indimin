
using System.Collections.Generic;
using Backend.Domain.Common;
using Backend.Domain.Event;

namespace Backend.Domain.Entities
{
    public class AssignmentByCitizen : AuditableEntity, IHasDomainEvent
    {
        public AssignmentByCitizen()
        {
            DomainEvents = new List<DomainEvent>();
        }
        public int Id { get; set; }
        public int AssignmentId { get; set; }
        public int CitizenId { get; set; }
        public int Day { get; set; }
        public string Status { get; set; }
        public List<DomainEvent> DomainEvents { get; set; }
        public Assignment Assignment{get;set;}
        public Citizen Citizen{get;set;}
    }
}
