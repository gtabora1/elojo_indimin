
using System.Collections.Generic;
using Backend.Domain.Common;
using Backend.Domain.Event;

namespace Backend.Domain.Entities
{
    public class Citizen : AuditableEntity, IHasDomainEvent
    {
        public Citizen()
        {
            DomainEvents = new List<DomainEvent>();
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Active { get; set; }
        public List<DomainEvent> DomainEvents { get; set; }
        public List<Assignment> Assignments { get; set; }
    }
}
