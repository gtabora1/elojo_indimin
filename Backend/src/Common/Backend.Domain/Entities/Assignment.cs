
using System.Collections.Generic;
using Backend.Domain.Common;

namespace Backend.Domain.Entities
{
    public class Assignment : AuditableEntity, IHasDomainEvent
    {
        public Assignment()
        {
            DomainEvents=new List<DomainEvent>();
        }
        public int Id { get; set; }
        public string Title{get;set;}
        public string Description { get; set; }
        public bool Active { get; set; }
        public List<DomainEvent> DomainEvents { get;set; }
    }
}
