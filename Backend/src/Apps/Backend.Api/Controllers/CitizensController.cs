using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Citizens.Commands.Create;
using Backend.Application.Citizens.Commands.Delete;
using Backend.Application.Citizens.Commands.Update;
using Backend.Application.Citizens.Queries.GetCitizen;
using Backend.Application.Citizens.Queries.GetCitizenById;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Api.Controllers
{
   
    public class CitizensController : BaseApiController
    {
        [EnableCors]
        [HttpGet]
        public async Task<ActionResult<ServiceResult<List<CitizenDto>>>> GetAllCitizens(CancellationToken cancellationToken)
        {
            //Cancellation token example.
            return Ok(await Mediator.Send(new GetAllCitizensQuery(), cancellationToken));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ServiceResult<CitizenDto>>> GetCitizen(int id)
        {
            return Ok(await Mediator.Send(new GetCitizenByIdQuery { Id = id }));
        }

        [HttpPost]
        public async Task<ActionResult<ServiceResult<CitizenDto>>> Create(CreateCitizenCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut]
        public async Task<ActionResult<ServiceResult<CitizenDto>>> Update(UpdateCitizenCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<ServiceResult<CitizenDto>>> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteCitizenCommand { Id = id }));
        }
    }
}
