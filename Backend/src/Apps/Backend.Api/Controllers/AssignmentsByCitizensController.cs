using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.AssigmentByCitizen.Queries.GetAssignmentsByCitizens;
using Backend.Application.AssignmentsByCitizens.Commands.Create;
using Backend.Application.AssignmentsByCitizens.Commands.Delete;
using Backend.Application.AssignmentsByCitizens.Commands.Update;
using Backend.Application.AssignmentsByCitizens.Queries.GetAssignmentsPerDay;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Api.Controllers
{
    public class AssignmentsByCitizensController : BaseApiController
    {
        [HttpGet]
        public async Task<ActionResult<ServiceResult<List<AssignmentByCitizenDto>>>> GetAllAssignmentsByCitizens(CancellationToken cancellationToken)
        {
            //Cancellation token example.
            return Ok(await Mediator.Send(new GetAllAssignmentsByCitizensQuery(), cancellationToken));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ServiceResult<AssignmentDto>>> GetAssignmentsPerDay(int day)
        {
            return Ok(await Mediator.Send(new GetAssignmentsPerDayQuery() { Day = day }));
        }

        [HttpPost]
        public async Task<ActionResult<ServiceResult<AssignmentDto>>> Create(CreateAssignmentByCitizenCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut]
        public async Task<ActionResult<ServiceResult<AssignmentDto>>> Update(UpdateAssignmentByCitizenCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<ServiceResult<AssignmentDto>>> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteAssignmentsByCitizensCommand { Id = id }));
        }
    }
}
