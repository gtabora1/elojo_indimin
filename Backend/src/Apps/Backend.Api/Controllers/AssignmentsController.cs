using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Assignments.Commands.Create;
using Backend.Application.Assignments.Commands.Delete;
using Backend.Application.Assignments.Commands.Update;
using Backend.Application.Assignments.Queries.GetAssignmentById;
using Backend.Application.Assignments.Queries.GetAssignments;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Api.Controllers
{
    public class AssignmentsController : BaseApiController
    {
        [HttpGet]
        public async Task<ActionResult<ServiceResult<List<AssignmentDto>>>> GetAllAssignments(CancellationToken cancellationToken)
        {
            //Cancellation token example.
            return Ok(await Mediator.Send(new GetAllAssignmentsQuery(), cancellationToken));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ServiceResult<AssignmentDto>>> GetAssignment(int id)
        {
            return Ok(await Mediator.Send(new GetAssignmentByIdQuery { Id = id }));
        }

        [HttpPost]
        public async Task<ActionResult<ServiceResult<AssignmentDto>>> Create(CreateAssignmentCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut]
        public async Task<ActionResult<ServiceResult<AssignmentDto>>> Update(UpdateAssignmentCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<ServiceResult<AssignmentDto>>> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteAssignmentCommand { Id = id }));
        }
    }
}
